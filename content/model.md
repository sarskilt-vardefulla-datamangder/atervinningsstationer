# Datamodell

Datamodellen är tabulär, det innebär att varje rad motsvarar exakt en entitet och varje kolumn motsvarar en egenskap för den beskrivna entiteten. 19  attribut är definierade, där de första 7 är obligatoriska. Speciellt viktigt är att man anger de obligatoriska fält i datamodellen som anges. 


<div class="note" title="1">
Vi väljer att använda beskrivande men korta kolumnnamn som uttrycks med gemener, utan mellanslag (understreck för att separera ord) och på engelska.

Genom engelska attributnamn blir modellen enklare att hantera i programvaror och tjänster utvecklade utanför Sverige.


</div>

<div class="ms_datatable">

| Namn  | Kardinalitet      | Datatyp                         | Beskrivning|
| -- | :---------------:| :-------------------------------: | ---------------------- |
|[**source**](#source)|1|[heltal](#heltal)|**Obligatoriskt** - Organisationsnummer för organisationen som ansvarar för Återvinningsstationen.|
|[**id**](#id)|1|text|**Obligatoriskt** - En unik identifierare för Återvinningsstationen.|
|[**name**](#name)|1|text|**Obligatoriskt** - Återvinningsstationens namn.|
|[**latitude**](#latitude)|1|[decimal](#decimal)|**Obligatoriskt** - Geografisk latitud för återvinningsstationens plats, angiven i decimalformat.|
|[**longitude**](#longitude)|1|[decimal](#decimal)|**Obligatoriskt** - Geografisk longitud för återvinningsstationens plats, angiven i decimalformat.|
|[**municipality_code**](#municipality_code)|1|[heltal](#heltal)|**Obligatoriskt** - Kommunens kod, där återvinningsstationen är belägen.|
|[**municipality**](#municipality)|1|[text](#text)|**Obligatoriskt** - Namnet på den kommun där återvinningsstationen är belägen.|
|[street](#street)|0..1|[text](#text)|Gatan där återvinningsstationen är belägen.|
|[postalcode](#postalcode)|0..1|[heltal](#heltal)|Postnummer för Återvinningsstationen.|
|[property_id](#property_id)|0..1|text|Fastighetsbeteckningen för Återvinningstationen.|
|[email](#email)|0..1|text|E-postadress, till organisationen ansvarig för Återvinningsstationen.|
|[URL](#url)|0..1|[URL](#url)|Ingångssida för organisationen som ansvarar för Återvinningsstationen.|
|[waste_category](#waste_category)|0..n|[concept](#concept)|Avfallskategori, avser vilken kategori av avfall som kan hanteras på Återvinningstationen.|
|[service_type](#service_type)|0..n|[heltal](#heltal)|Kod för beskrivning av vilka tjänster som finns på Återvinningsstationen.|
|[last_action](#last_action)|0..1|[datetime](#datetime)|Datum för senaste åtgärd.|
|[last_action_text](#last_action_text)|0..1|text|Tydlig beskrivning av senaste åtgärd på Återvinningsstationen. Exempelvis "Senaste tömning"|
|[next_action](#next_action)|0..1|[datetime](#datetime)|Datum för nästa åtgärd. Se förtydligande av dataformat nedan.|
|[next_action_text](#next_action_text)|0..1|text|Tydlig beskrivning av kommande åtgärd på Återvinningsstationen. Exempelvis "Nästa tömning"
|[description](#description)|0..1|text|Ytterligare beskrivning av Återvinningsstationen.|

</div>

## Förtydligande av datatyper

En del av datatyperna nedan förtydligas med hjälp av det som kallas reguljära uttryck. Dessa är uttryckta så att de matchar exakt, dvs inga inledande eller eftersläpande tecken tillåts.

### **heltal**
Reguljärt uttryck: **`/^\-?\\d+$/`**

Heltal anges alltid som en radda siffror utan mellanrum eventuellt med ett inledande minus. Se [xsd:integer](https://www.w3.org/TR/xmlschema-2/#integer) för en längre definition. Utelämnat värde tolkas aldrig som noll (0) utan tolkas som “avsaknad av värde”.
 
### **decimal**
Reguljärt uttryck: **`/^\-?\\d+\\.\\d+$/`**

Decimaltal anges i enlighet med [xsd:decimal](https://www.w3.org/TR/xmlschema-2/#decimal). Notera att i Sverige används ofta decimalkomma inte punkt. För att vara enhetlig mellan olika dataformat ska decimalpunkt användas då den tabulära modellen använder komma som separator.

Den kanoniska representationen i xsd:decimal är påbjuden, d.v.s. inga inledande nollor eller +, samt att man alltid ska ha en siffra innan och efter decimalpunkt. Noll skrivs som 0.0 och ett utelämnat värde skall aldrig tolkas som noll (0) utan “avsaknad av värde”.

### **url**

En länk till en webbsida där entiteten presenteras hos den lokala myndigheten eller organisationen.

Observera att man inte får utelämna schemat, d.v.s. "www.example.com" är inte en tillåten webbadress, däremot är "https://www.example.com" ok. Relativa webbadresser accepteras inte heller. (Ett fullständigt reguljärt uttryck utelämnas då den är både för omfattande och opedagogisk.)

Om du behöver ange flera URL:er måste du då sätta dubbelt citattecken och separera de olika URL:erna med kommatecken. Läs gärna med i RFC 4180 för CSV.

Kontrollera även innan du publicerar filen att det går att läsa in din fil utan problem. Det finns flera webbverktyg för att testa så kallad [parsing](https://sv.wikipedia.org/wiki/Parser) t.ex. [https://CSVLint.io](https://csvlint.io) som du kan använda för att testa din fil.

### **boolean**

Reguljärt uttryck: **`/^[true|false]?&/`**

I samtliga förekommande fall kan texten “true” eller “false” utelämnas.

Ett tomt fält skall tolkas som “okänt” eller “ej inventerat”. Ange enbart “true” eller “false” om du vet att egenskapen finns (true) eller saknas (false). Gissa aldrig.

Attributet är en så kallad Boolesk” datatyp och kan antingen ha ett av två värden: TRUE eller FALSE, men aldrig båda.

### **dateTime** 

Reguljärt uttryck: **`\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d(?:\.\d+)?Z?`**

dateTime värden kan ses som objekt med heltalsvärdena år, månad, dag, timme och minut, en decimalvärderad sekundegenskap och en tidszonegenskap.

[XML schema](https://www.w3.org/TR/xmlschema-2/#dateTime) 
</br>

[IS0 8601](https://en.wikipedia.org/wiki/ISO_8601)

Värden som kan anges för att beskriva ett datum:

YYYY-MM-DD
</br>
YYYY-MM
</br>
YYYYMMDD 
</br>
</div>

Observera att ni inte kan ange: **YYYYMM**    

För att beskriva tider uttrycker ni det enligt nedan:

**T** representerar tid, **hh** avser en nollställd timme mellan 00 och 24, **mm** avser en nollställd minut mellan 00 och 59 och **ss** avser en nollställd sekund mellan 00 och 60.

Thh:mm:ss.sss eller	Thhmmss.sss 
</br>
Thh:mm:ss eller Thhmmss
</br>
Thh:mm eller Thhmm
</br>

</div>

Exempel - XX som äger rum kl 17:00 den 22 februari 2023 

</br>
2024-02-22T17:00

</br>
</br>

20240222T1700

</div>

## Förtydligande av attribut


### source

Organisationsnummer till den organisation eller myndighet som ansvarar för och tillhandahåller informationen om återvinningsstationen. Detta anger vem som ansvarar för att samla in och publicera datan. Exempel för Gullspångs kommun: **2120001637**.

### id

Ett unikt numeriskt värde som identifierar varje enskild återvinningsstation i datamängden. Det viktiga är att identifieraren är unik (tänk unik i hela världen), och stabil över tid. Dvs att den inte ändras mellan uppdateringar.

### name

Det specifika namnet på återvinningsstationen, som kan vara officiellt eller informellt. Namnet hjälper användare att identifiera stationen.
Exempelvis "Klockarvägen".

### latitude

Latitud anges per format som WGS84-specifikation. WGS84 är den standard som det Amerikanska systemet GPS använder för att beskriva en latitud på jordklotets yta. Latitud anges med ett heltal följt av en decimalpunkt “.” och 1 till 8 decimaler. Exempelvis “61.21657”. En latitudangivelse som befinner sig på jordens södra hemisfär anges med negativt tal. Exempelvis “-53.78589”. Om koordinatens inledande heltal är noll, skall alltid nollan anges. 

### longitude

Longitud anges per format som WGS84. Longitud anges med ett heltal följt av en decimalpunkt följt av 1 till 8 decimaler. Exempelvis “88.40901”. En longitud som ligger väster om WGS84-systemets meridian, anges med negativt tal, Exempelvis: “-0.158101”. Om koordinatens inledande heltal är noll, skall nollan alltid anges.
Tillsammans med latituden används den för exakt kartläggning.

### municipality_code

 En unik kod som identifierar vilken kommun återvinningsstationen tillhör. Varje kommun i ett land har en specifik kod som används för administrativa och statistiska ändamål. Detta attribut gör det möjligt att koppla återvinningsstationen till en specifik lokal förvaltning, vilket kan vara viktigt för regional planering och rapportering. Använd befintliga kommunkoder, definierade av SCB. Listor med aktuella och officiella kommunkoder hittar ni på SCB's hemsida. Exempelvis "0181".


### municipality

Namnet på den kommun där återvinningsstationen är belägen. Kommunen är den lokala förvaltningsenheten som ansvarar för tjänster som avfallshantering och återvinning. Namnet gör det tydligt för användare vilken lokal förvaltning som ansvarar för återvinningsstationen och dess tjänster.
Kommunnamnet skrivs med fördel i kortform, exempelvis "Södertälje". Dvs inte med tillägget "kommun" som i "Södertälje kommun". Detta i enlighet med Eurostat och ett antal SOS-myndigheter (producenter av Sveriges Officiella Statistik).


### street

Gatuadressen till återvinningsstationen, inklusive gatunamn och gatunummer. Adressen gör det möjligt för användare att enkelt hitta platsen, särskilt för dem som inte använder GPS. Exempelvis "Återbruksgatan 12"


### postalcode

Postnumret för området där återvinningsstationen är belägen. Detta hjälper till att geografiskt avgränsa stationens plats. Ange inte mellanslag. Exempelvis "15132"

### property_id

Den juridiska eller administrativa beteckningen för den fastighet där återvinningsstationen är placerad. Detta används för att referera till fastighetsinformation.

### email

Anger funktionsadress till den organisation som ansvarar för information om Återvinningsstationen, för vidare kontakt. Ange ej det inledande URI schemat mailto: eller HTML-koder i uttrycket. Exempel: example@domain.se

### URL

En webblänk till ytterligare information om återvinningsstationen, till exempel på kommunens hemsida eller återvinningsföretagets sida. Användare kan besöka den för att få mer detaljer om stationens tjänster eller öppettider Ange inledande schemat https:// eller http://

### waste_category

 En lista över de olika typer av material och avfall som återvinningsstationen accepterar, såsom plast, metall, papper eller elektronik. Detta hjälper användare att veta vad som kan lämnas in vid stationen. Följer [EU-standard och vokabulär](https://op.europa.eu/en/web/eu-vocabularies/concept-scheme/-/resource?uri=http://data.europa.eu/6p8/waste/scheme) och det är dessa koder som ska användas för detta attribut

 - Här anger ni värdet från kolumnen code utan item, exempelvis **1**. 

### service_type

  Tjänster avser tjänster som genomförs på ÅVS. Exempelvis "Städning" och "Vinterhållning".

### last_action_date

Det senaste datumet då en åtgärd, som till exempel insamling, rengöring eller service, genomfördes på återvinningsstationen. Detta kan användas för att spåra underhållshistorik.

### last_action_text

En beskrivande text som förklarar vad den senaste åtgärden på stationen var, såsom "tömning av containrar" eller "underhåll utfört".

### next_action_date

Datumet för när nästa planerade åtgärd eller inspektion på återvinningsstationen kommer att ske. Detta kan vara viktigt för att planera underhåll eller service.

### next_action_text

En beskrivning av vad den kommande åtgärden innebär, exempelvis "tömning av glascontainrar" eller "reparation av staket". Denna information ger insyn i framtida underhåll.

### description

Ytterligare beskrivning av Återvinningsstationen.