# Exempel i JSON

Samma exempel som i Appendix A, fast nu som JSON.

```json
{
    "source": "556031-4546",
    "id": "ÅVS123",
    "name": "Återvinningsstation Bergsjön",
    "latitude": 57.758875,
    "longitude": 12.000353,
    "municipality_code": "1480",
    "municipality": "Göteborg",
    "street": "Bergsjövägen 42",
    "postalcode": "41521",
    "property_id": "Bergsjön 2:14",
    "email": "kontakt@ftiab.se",
    "URL": "https://www.ftiab.se",
    "waste_category": "15,17,20,22,40",
    "service_type": "1",
    "last_action": "2024-11-17T08:30:00",
    "last_action_text": "Tömning av containrar",
    "next_action": "2024-11-24T08:30:00",
    "next_action_text": "Planerad tömning",
    "description": "Återvinningsstation vid Bergsjö centrum med containrar för glas (15), metall (17), papper (20), plast (22) och tidningar (40)"
}
```
