## Exempel i CSV, kommaseparerad

<div class="example csvtext">

source,id,name,latitude,longitude,municipality_code,municipality,street,postalcode,property_id,email,URL,waste_category,service_type,last_action,last_action_text,next_action,next_action_text,description
<br>
556031-4546,ÅVS123,Återvinningsstation Bergsjön,57.758875,12.000353,1480,Göteborg,Bergsjövägen 42,41521,Bergsjön 2:14,kontakt@ftiab.se,https://www.ftiab.se, 15,17,20,22,40,1,2024-11-17T08:30:00,Tömning av containrar,2024-11-24T08:30:00,Planerad tömning,Återvinningsstation vid Bergsjö centrum med containrar för glas, metall, plast och papper
</div>

