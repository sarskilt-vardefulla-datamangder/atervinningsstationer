**Bakgrund**

Syftet med denna specifikation är att beskriva information om återvinningsstationer på ett enhetligt och standardiserat vis. 

Att ta fram en specifikation för datamängden återvinningsstationer innebär att standardisera hur information om dessa stationer struktureras och tillgängliggörs. Syftet är att göra det lättare för olika aktörer att få tillgång till enhetlig och tillförlitlig information, vilket underlättar jämförelser och analyser, samt möjliggör utveckling av digitala tjänster som kan hjälpa medborgare att hitta närmaste station och förstå vilka material som accepteras. Standardisering säkerställer också att olika system kan samverka smidigt, vilket förbättrar interoperabiliteten och gör datan mer användbar för en bredare publik. Genom att tillgängliggöra datan som öppen data främjas innovation, miljömedvetenhet och ansvarstagande, och det blir enklare att effektivisera resurshantering på både lokal och nationell nivå. Samtidigt säkerställs en konsekvent kvalitet och tydlighet i informationen, vilket underlättar för myndigheter, företag och medborgare att bidra till ett mer hållbart samhälle.

Följande har deltagit:

**[Dataverkstad](https://www.vgregion.se/ov/dataverkstad/)** - Modellering och rådgivning.<br>



