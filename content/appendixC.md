# Exempel i JSON

Samma exempel som i Appendix A, fast nu som JSON-LD.

```json
{
  "@context": {
    "schema": "http://schema.org/",
    "dct": "http://purl.org/dc/terms/",
    "rdf": "http://www.w3.org/1999/02/22-rdf-syntax-ns#",
    "cv": "http://www.cadastralvocabulary.org/",
    "prov": "http://www.w3.org/ns/prov#",
    "xsd": "http://www.w3.org/2001/XMLSchema#"
  },
  "@type": "schema:RecyclingCenter",
  "@id": "ÅVS123-3421",
  "dct:source": "556031-4546",
  "dct:identifier": "ÅVS123",
  "schema:name": {
    "@language": "sv",
    "@value": "Återvinningsstation Bergsjön"
  },
  "schema:geo": {
    "@type": "schema:GeoCoordinates",
    "schema:latitude": {
      "@type": "xsd:decimal",
      "@value": 57.758875
    },
    "schema:longitude": {
      "@type": "xsd:decimal",
      "@value": 12.000353
    }
  },
  "schema:identifier": {
    "@type": "xsd:integer",
    "@value": 1480
  },
  "schema:address": {
    "@type": "schema:PostalAddress",
    "schema:streetAddress": "Bergsjövägen 42",
    "schema:postalCode": "41521",
    "schema:addressLocality": "Göteborg"
  },
  "cv:nationalCadastralReference": "Bergsjön 2:14",
  "schema:email": "kontakt@ftiab.se",
  "schema:url": {
    "@id": "https://www.ftiab.se"
  },
  "rdf:type": [
    { "@id": "http://data.europa.eu/6p8/waste/scheme/item15" },
    { "@id": "http://data.europa.eu/6p8/waste/scheme/item17" },
    { "@id": "http://data.europa.eu/6p8/waste/scheme/item20" },
    { "@id": "http://data.europa.eu/6p8/waste/scheme/item22" },
    { "@id": "http://data.europa.eu/6p8/waste/scheme/item40" }
  ],
  "schema:serviceType": {
    "@type": "xsd:integer",
    "@value": 1
  },
  "prov:hadActivity": {
    "@type": "prov:Activity",
    "schema:dateModified": {
      "@type": "xsd:dateTime",
      "@value": "2024-11-17T08:30:00"
    },
    "dct:description": "Tömning av containrar"
  },
  "prov:activity": {
    "@type": "prov:Activity",
    "schema:scheduledTime": {
      "@type": "xsd:dateTime",
      "@value": "2024-11-24T08:30:00"
    },
    "dct:description": "Planerad tömning"
  },
  "dct:description": {
    "@language": "sv",
    "@value": "Återvinningsstation vid Bergsjö centrum med containrar för glas (15), metall (17), papper (20), plast (22) och tidningar (40)"
  }
}
``` 