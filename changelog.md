# Version 0.9 (2024-10-18)

## Fixed
- No fixes noted in this version.

## Changed

### content/model.md
- Updated the number of attributes: now 19 attributes are defined, with the first 7 being obligatory.
- Renamed "property_designation" to "property_id".

### schema.json
- Refined the overall schema structure for Återvinningsstationer (recycling stations).
- Updated metadata with specific keywords and modification date.
- Modified data types and descriptions for existing fields.
- Removed fields: "type", "city".
- Changed "property_designation" to "property_id".

## Added

### content/model.md
- No new additions noted.

### schema.json
- New fields:
  - source (organization number)
  - name (of the recycling station)
  - municipality_code
  - municipality
  - waste_category
  - service_type
  - last_action (date and description)
  - next_action (date and description)
- Semantic web vocabularies (schema.org, Dublin Core, etc.) to map fields to standardized properties.
- Primary key definition: "id".
- About URL definition.

## Removed
### content/model.md
- Removed the "housenumber" attribute.

### schema.json
- Removed fields: "type", "city".