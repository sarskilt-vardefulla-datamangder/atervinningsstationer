{
  "@context": [
    "http://www.w3.org/ns/csvw",
    {
      "@language": "sv",
      "dc:title": "CSV schema för Återvinningsstationer",
      "dcat:keyword": ["atervinningsstationer"],
      "schema": "http://schema.org/",
      "dct": "http://purl.org/dc/terms/",
      "rdf": "http://www.w3.org/1999/02/22-rdf-syntax-ns#",
      "cv": "http://www.cadastralvocabulary.org/",
      "prov": "http://www.w3.org/ns/prov#" 
    }
  ],
  "dc:license": {"@id": "http://opendefinition.org/licenses/cc-by/"},
  "dc:modified": {"@value": "2024-18-11", "@type": "xsd:date"},
  "tableSchema": {
    "columns": [
      {
        "name": "source",
        "titles": "Källa",
        "dc:description": "Organisationsnummer för organisationen som ansvarar för Återvinningsstationen.",
        "datatype": "integer",
        "required": true,
        "propertyUrl": "dct:source"
      },
      {
        "name": "id",
        "titles": "Objektidentitet",
        "dc:description": "En unik identifierare för Återvinningsstationen.",
        "datatype": "string",
        "required": true,
        "propertyUrl": "dct:identifier"
      },
      {
        "name": "name",
        "titles": "Namn",
        "dc:description": "Återvinningsstationens namn.",
        "datatype": "string",
        "required": true,
        "propertyUrl": "schema:name"
      },
      {
        "name": "latitude",
        "titles": "Latitud",
        "dc:description": "Geografisk latitud för återvinningsstationens plats, angiven i decimalformat.",
        "datatype": "decimal",
        "required": true,
        "propertyUrl": "schema:latitude"
      },
      {
        "name": "longitude",
        "titles": "Longitud",
        "dc:description": "Geografisk longitud för återvinningsstationens plats, angiven i decimalformat.",
        "datatype": "decimal",
        "required": true,
        "propertyUrl": "schema:longitude"
      },
      {
        "name": "municipality_code",
        "titles": "Kommunkod",
        "dc:description": "Kommunens kod, som den inrapporterade avfallsinformationen avser.",
        "datatype": "integer",
        "required": true,
        "propertyUrl": "schema:identifier"
      },
      {
        "name": "municipality",
        "titles": "Kommun",
        "dc:description": "Namnet på den kommun där återvinningsstationen är belägen.",
        "datatype": "string",
        "required": true,
        "propertyUrl": "schema:addressLocality"
      },
      {
        "name": "street",
        "titles": "Gatuadress",
        "dc:description": "Gatuadress där återvinningsstationen är belägen.",
        "datatype": "string",
        "propertyUrl": "schema:streetAddress"
      },
      {
        "name": "postalcode",
        "titles": "Postnummer",
        "dc:description": "Postnummer för Återvinningsstationen.",
        "datatype": "integer",
        "propertyUrl": "schema:postalCode"
      },
      {
        "name": "property_id",
        "titles": "Fastighetsbeteckning",
        "dc:description": "Fastighetsbeteckningen för Återvinningstationen.",
        "datatype": "string",
        "propertyUrl": "cv:nationalCadastralReference"
      },
      {
        "name": "email",
        "titles": "E-post",
        "dc:description": "E-postadress, till organisationen ansvarig för Återvinningsstationen.",
        "datatype": "string",
        "propertyUrl": "schema:email"
      },
      {
        "name": "URL",
        "titles": "Ingångssida",
        "dc:description": "Ingångssida för organisationen som ansvarar för Återvinningsstationen.",
        "datatype": "string",
        "propertyUrl": "schema:url"
      },
      {
        "name": "waste_category",
        "titles": "Typ av material",
        "dc:description": "Det återvunna materialets typ.",
        "datatype": {
          "base": "integer",
          "minimum": 1,
          "maximum": 49
          },
        "required": true,
        "valueUrl": "http://data.europa.eu/6p8/waste/scheme/item{waste_category}",
        "propertyUrl": "rdf:type"
      }, 
      {
        "name": "service_type",
        "titles": "Tjänstetyp",
        "dc:description": "Kod för beskrivning av vilka tjänster som finns på Återvinningsstationen.",
        "datatype": "integer",
        "propertyUrl": "schema:serviceType"
      },
      {
        "name": "last_action",
        "titles": "Senaste åtgärd datum",
        "dc:description": "Datum för senaste åtgärd.",
        "datatype": "datetime",
        "propertyUrl": "schema:dateModified"
      },
      {
        "name": "last_action_text",
        "titles": "Senaste åtgärd beskrivning",
        "dc:description": "Tydlig beskrivning av senaste åtgärd på Återvinningsstationen. Exempelvis 'Senaste tömning'",
        "datatype": "string",
        "propertyUrl": "prov:hadActivity"
      },
      {
        "name": "next_action",
        "titles": "Nästa åtgärd datum",
        "dc:description": "Datum för nästa åtgärd.",
        "datatype": "datetime",
        "propertyUrl": "schema:scheduledTime"
      },
      {
        "name": "next_action_text",
        "titles": "Nästa åtgärd beskrivning",
        "dc:description": "Tydlig beskrivning av kommande åtgärd på Återvinningsstationen. Exempelvis 'Nästa tömning'",
        "datatype": "string",
        "propertyUrl": "prov:activity"
      },
      {
        "name": "description",
        "titles": "Beskrivning",
        "dc:description": "Ytterligare beskrivning av Återvinningsstationen.",
        "datatype": "string",
        "propertyUrl": "dct:description"
      }
    ],
    "primaryKey": "id",
    "aboutUrl": "#gid-{id}"
  }
}